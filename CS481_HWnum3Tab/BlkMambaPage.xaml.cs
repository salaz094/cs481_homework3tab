﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HWnum3Tab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BlkMambaPage : ContentPage
    {
        public BlkMambaPage()
        {
            InitializeComponent();
        }

         protected async override void OnAppearing()
        {
            base.OnAppearing();
            await DisplayAlert("Seasoned Kobe", "#24's Achievements", "Ok"); //intro to page
        }

        protected async override void OnDisappearing()
        {
            base.OnDisappearing();
            await DisplayAlert("The end", "Kobe retired April 13, 2016 after dropping 60 in his final game", "Continue"); //as tab is left
        }
    }
}