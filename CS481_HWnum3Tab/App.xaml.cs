﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CS481_HWnum3Tab.Services;
using CS481_HWnum3Tab.Views;

//Adapted from:https://github.com/xamarin/xamarin-forms-samples/tree/master/Navigation/TabbedPageWithNavigationPage/TabbedPageWithNavigationPage

namespace CS481_HWnum3Tab
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
