﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HWnum3Tab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FrobePage : ContentPage
    {
        public FrobePage()
        {
            InitializeComponent();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await DisplayAlert("Young Kobe", "#8's Achievements", "Ok"); //Refering to Image of young kobes accolades in background 
        }

        protected async override void OnDisappearing()
        {
            base.OnDisappearing();
            await DisplayAlert("Next Chapter", "Kobe changed to #24 at the start of the 06-07 season.", "Continue"); //as tab is left
        }
    }
}