﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CS481_HWnum3Tab
{
    class BlkMambaClass : ContentPage
    {
		public BlkMambaClass()
		{


			Title = "Black Mamba";
			Content = new StackLayout
			{
				Children = {
					new Image {
						Source = "Kobe24.png",
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						VerticalOptions = LayoutOptions.CenterAndExpand
					}

				}
			};
		}
	}
}
