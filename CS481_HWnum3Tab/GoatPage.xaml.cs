﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HWnum3Tab
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GoatPage : ContentPage
    {
        public GoatPage()
        {
            InitializeComponent();
        }

        async void OnClicked(object sender, EventArgs e)
        {
            await DisplayAlert("24/8", "Only one Kobe", "Continue");
            var browser = new WebView();
            browser.Source = "https://twitter.com/Nike/status/1232002271266758661"; // opens up a twiter kobe tribute
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await DisplayAlert("Legends are forever", "Click button for a tribute", "Continue"); // when switching to the "goat" tab
        }
        //https://twitter.com/Nike/status/1232002271266758661
    }
}