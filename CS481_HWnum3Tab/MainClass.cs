﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CS481_HWnum3Tab
{
	public partial class MainClass : TabbedPage
	{
		public MainClass()
		{
			var navigationPage = new NavigationPage(new GoatClass());
			navigationPage.IconImageSource = "goat.png";
			navigationPage.Title = "MambaOut";

			Children.Add(new FrobeClass());
			Children.Add(new BlkMambaClass());
			Children.Add(navigationPage);

		}
	}
}
