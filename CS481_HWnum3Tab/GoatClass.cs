﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;


namespace CS481_HWnum3Tab
{
    class GoatClass : ContentPage
    {
		public GoatClass()
		{
			var TwitterVid = new Button
			{
				Text = "Mamba Forever",
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			
			Title = "GOAT";
			Content = new StackLayout
			{
				Children = {
					new Image {
						Source = "kobe4eva.png",
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						VerticalOptions = LayoutOptions.CenterAndExpand
					},TwitterVid

				}
			};
		}
	}
}
